window.googleSheet = (function ($, undefined) {
    getImageUrl = function (objectAsins, lang) {
        if(lang == "US") {
            return "https://ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&Format=_SX522_&ID=AsinImage&MarketPlace=" + lang + "&ServiceVersion=20070822&WS=1&ASIN=" + objectAsins;
        }else
        return "https://ws-eu.amazon-adsystem.com/widgets/q?_encoding=UTF8&Format=_SX522_&ID=AsinImage&MarketPlace=" + lang + "&ServiceVersion=20070822&WS=1&ASIN=" + objectAsins;
    }
    toEuros = function (priceString) {
        var regex = new RegExp("EUR (.+,.+)");
        if (regex.test(priceString)) {
            var match = regex.exec(priceString)
            if (match[1]) {
                return match[1] + "€"
            }
        }
        return priceString
    }
    getShortDesc = function (desc) {
        if (desc.indexOf("<br/>") != -1) {
            return desc.split("<br/>")[0];
        }
        else {
            return desc;
        }
    }
    // function getUrlFor(asin, amazonId, lang){
    //     if (lang == "US" ){
    //         return `https://ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&Operation=GetAdHtml&OneJS=1&source=ac&slotNum=0&ref=tf_til&tracking_id=${amazonId}&ad_type=product_link&marketplace=amazon&region=US&linkid=&asins=${asin}`;
    //     }else {
    //         return `https://ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&MarketPlace=${lang}&Operation=GetAdHtml&OneJS=1&source=ac&slotNum=0&ref=tf_til&tracking_id=${amazonId}&ad_type=product_link&marketplace=amazon&region=${lang}&linkid=&asins=${asin}`;
    //     }
    // }
    function getUrlFor(asin, amazonId, lang){
        return "https://store.recalbox.com/proxy/"+lang+"/"+amazonId+"/"+asin;
    }

    getInfoForObject = function (item, amazonId, lang) {
        return $.get(getUrlFor(item.asin, amazonId, lang))
            .done(function (data) {
                var html = $.parseHTML(data)
                var title = "", description = "", shortdescription = "";
                var shortdescription = getShortDesc(item.description);
                item.price = toEuros($("span.price", html).text());
                item.amazontitle = $("#title #titlehref", html).text();
                item.shortdescription = getShortDesc(item.description);
                item.link = $("#title #titlehref", html).attr("href");
                item.image = item.image || getImageUrl(item.asin, lang);
            }).fail(function (data) {
                item.error = {"error": "unable to get item informations"}
            })
    }

    return {
        loadBoutique: function (lang, callback) {
            let shopSheetUrl = "https://docs.google.com/spreadsheets/d/1xq-O66y6biLa67J2IR5D5CBJFRG4EEWKmYhZoJ4Urjc/pubhtml"
            $.get(shopSheetUrl).done(function (data) {
                var html = $.parseHTML(data),
                    sheets = [];
                $("#sheet-menu li", html).each(function (a) {
                    var lang = $("a", this).text().split("|")[0];
                    var amazonId = $("a", this).text().split("|")[1];
                    var id = $(this).attr("id").split("-")[2];
                    sheets.push({id: id, amazonId: amazonId, lang: lang, categories: []})
                });
                for (var sheetIndex in sheets) {
                    if (sheets[sheetIndex].lang != lang) {
                        continue;
                    }
                    var sheet = sheets[sheetIndex],
                        lines = $("#" + sheet.id + " tr", html),
                        currentCategory,
                        defered = [];
                    lines.each(function () {
                        var firstTD = $(this).find("td:first");
                        if (firstTD.hasClass("s1")) {
                            currentCategory = {name: firstTD.text(), items: []};
                            sheet.categories.push(currentCategory);
                            console.log(currentCategory.name)
                        } else if (firstTD.text().indexOf("B0") != -1) {
                            var item = {
                                asin: firstTD.text(),
                                title: firstTD.next().text(),
                                description: firstTD.next().next().text(),
                                image: firstTD.next().next().next().text()
                            };
                            currentCategory.items.push(item);
                            defered.push(getInfoForObject(item, sheet.amazonId, sheet.lang))
                        }
                    });
                    $.when.apply($, defered).done(function () {
                        console.log(sheet);
                        callback(sheet);
                    });
                }
            }).fail(function(error){
                $(".shopalert").css("display", "block")
                $('.shopalert').addClass('ns-show');
            })
        }
    }
})($);
