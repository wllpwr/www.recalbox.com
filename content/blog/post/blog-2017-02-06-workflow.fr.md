+++
date = "2017-02-06T13:28:50+02:00"
title = "Des nouvelles du projet !"
draft = false
image = "/images/blog/2017-02-06-workflow/mainimage.png"
disqus_identifier = "blog-2017-02-06-workflow"

[author]
    name = "digitalLumberjack"
    gitlab = "https://gitlab.com/digitallumberjack"
    github = "https://github.com/digitallumberjack"
    twitter = "https://twitter.com/digitalumberjak"
+++

<div>Un moment qu'on n'avait pas fait de point sur l'avancée du projet recalbox avec vous. On ne vous oublie pas ! <br>L'équipe travaille d'arrache-pied depuis de nombreuses semaines pour préparer l'avenir de Recalbox. En effet, nous sommes restés silencieux car nous nous sommes concentrés sur un truc pas super sexy, chronophage et énergivore, mais... inévitable et indispensable : une totale remise à plat de notre <b>processus de développement</b>.<br> <br>Pas très excitant dit comme ça, mais à terme, ça veut dire quoi :<ul class="list list--sign"><li>des mises à jour plus régulières</li><li>des mises à jour plus légères</li><li>une plus grande réactivité</li><li>nous n'aurons plus que deux versions : stable et unstable</li></ul>Fini donc les mois d'attentes pour passer d'une version à une autre, pour ajouter telle ou telle fonction... et ça, c'est cool ! .<br>Une petite version <b>4.0.1</b> sera d'ailleurs déployée d'ici <b>quelques jours/semaines</b> pour préparer votre Recalbox à ces nouveautés. Pensez à la faire lorsque vous la verrez passer !<br><br>Puis dans la foulée, la sortie de la très attendue version publique de la <b>4.1.0 unstable</b>, avec le support du Bluetooth natif, de nouveaux systèmes supportés et <b>une pléiade de surprises</b>.<br>Une version-test ouverte à tous ; on compte d'ailleurs sur vous pour participer activement au projet et remonter sur notre forum officiel les éventuels bugs/amélioration.<br><br>Encore une fois, on tient sincèrement à <b>vous remercier</b> pour tout l'entrain et la fidélité dont vous faites preuve à l'égard de notre projet.<br><br>A bientôt !<img src="/images/blog/2017-02-06-workflow/chunli.gif"><br><br><p>Et au fait, nôtre blog est de retour :P</p></div>