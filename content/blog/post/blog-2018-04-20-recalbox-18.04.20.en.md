+++
date = "2018-04-20T01:00:00+02:00"
image = "/images/blog/2018-04-20-recalbox-18-04-20/recalbox-18.04.20-banner.png"
title = "Recalbox 18.04.20"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"

+++

Hey everyone!

We've been keeping on working on Recalbox lately to bring some new features, with again many changes to EmulationStation. You'll notice we've been working on a better internationalization of Recalbox by updating translations done by many volunteers. If you ever want to help [join here for emulationStation](https://poeditor.com/join/project/hEp5Khj4Ck) or [here for the webmanager](https://poeditor.com/join/project/taFNFlZ840).

We've also spend quite some time on better sound configuration. This involves dynamic volume change but even better: dynamic sound card output! Yeah yeah, you can now switch on-the-fly your audio device! The volume bug on update is still not fixed, but I have good hopes for the next release!

So, here we go with the detailled changelog:

* **ES: Added pop-ups** For now we just display the name of the music file being played, but the base is here, this is a feature we will improve and use more and more

* **ES: Added tag in theme to force transition style** Some themes (like fuzion) look better with some specific transitions. There is now a tag to force the transition style in the themes.

* **Bump kodi-plugin-video-youtube to 5.4.6** Just to have it updated, the previous one is not available anymore and can cause some compilation error

* **Bump Advancemame to 3.7 and spinner support added** A small update to AdvanceMAME, which now has mouse support enabled. That's a good thing for spinners, and probably for lightguns

* **ES: Fixed audio card change** This one, you'll love it ! That's the dynamic audio output change from ES. I think x86 users will pretty much enjoy this one ;)

* **fix: default font for ES and RA now in system** Remember the yellow squares bug ? That's to prevent it from coming back as some fonts had their path changed

* **ES: Added dynamic change of volume and audio output in menu** Self explanatory, uh ? No need to leave the window to hear the new sound level, it's dynamic when you move the slider!

* **ES: Added ubuntu_condensed.ttf as fallback font** This should help with non latin alphabets. We still need to find a font that can handle Korean though

* **ES: Added clock in main menu** Press start to see the time now!

* **ES: Added missing fav icons for Amigas 3DO and X68k** Can't say better I guess...

* **ES: Fixed reload of theme when gamelistonly=0** the theme reload should be a little faster

* **ES: Fixed HelpMessages not translated** Well ... Looks like an April's fool, translation still can't work on that part :/

* **ES: Add "QUIT" help message on main menu** The SELECT button had no tip of its role on the system view until now

* **revert PPSSPP version to previous one** due to massive demand regarding the 1.5.4 version that was causing major trouble, it's been rolledback to the previous version

* **more informations in support archives** That's for us, to help you!

* **upgrade: fix upgrading from custom branch to stable** We couldn't easily upgrade test branches without some manual edits. Now we can!

* **recallog now logs even when share is not mounted** So much better to know what happens during boot, even if the Recalbox logs are not ready. That's something we're gonna improve even more in the next releases

* **fix: wifi always activated on reboot even if disabled in ES** Wifi was always started at boot even if it was disabled. Bummer...

* **split share detection and upgrade process** That was one boot script doing too much work. The 2 main processes are now split in 2 scripts, that's easier for us to maintain

* **mame2010: enable hiscores** Woot! But ratios are still not yet solved.

* **fix: fmsx could not be selected as a MSX core** Now you can!

* **DosBox: resolve lag regression on RPI3 + strange behavior if no dosbox.bat** It works again like a charm!

* **SDL2: fix bug on x86 which stucks ScummVM on splash screen** Now you can play the games!

* **Odroid XU4: improved HDMI stability, boot.ini options** People using the XU4 on a HDMI CEC TV could face various issues. Should be stable now

* **fix: Game&Watch core name not properly displayed** Can't say better!

* **manager: update translations** The web manager has some updated translations!

Pretty nice uh ? 
