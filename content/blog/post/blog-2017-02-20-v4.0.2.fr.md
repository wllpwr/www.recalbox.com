+++
date = "2017-02-20T17:12:34+02:00"
image = "https://s3-eu-west-1.amazonaws.com/recalbox-team/4.0.2.jpg"
title = "RecalboxOS v4.0.2 est sortie !"
disqus_identifier = "blog-2017-02-20-v4.0.2"
draft = false

[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"

+++
<div>Une nouvelle version de recalbox est en ligne!<p dir="auto">La mise à jour 4.0.2 corrige <strong>le problème d'écran noir</strong> qui pouvait apparaître au lancement de jeux sur certains systèmes. Ce problème est du a dérnière version du firmware raspberry pi embarqué dans recalboxOS 4.0.1 qui ajoutait le support du Raspberry Pi 2v1.2.</p><p dir="auto">Vous pouvez maintenant continuer a battre des records sur Blade Buster !</p><p dir="auto">Pensez à faire vôtre mise à jour depuis le menu de vôtre recalbox. <br>A bientôt :)</p></div>