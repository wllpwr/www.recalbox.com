+++
date = "2017-10-15T16:46:06+02:00"
image = "/images/blog/2017-10-15-after-4.1/gitlab.jpg"
title = "Após a 4.1?"

[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"

[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"

+++

O Recalbox 4.1 está disponível publicamente desde Sexta-feira, 13 de Outubro as 20:00 CEST(17:00 GMT-3).

Enquanto escrevo estas linhas, vocês já baixaram mais de **17,000** imagens para novas instalações, e atualizaram **10000+** recalboxes!

![](/images/blog/2017-10-15-after-4.1/download-4.1.png)

Obrigado por compartilharem este momento conosco, vocês são a melhor comunidade que poderiamos ter sonhado!

#### Mudando do Github para o Gitlab
Há muitas alterações no Recalbox 4.1, e muitas não estão incluídas no SO, mas nas ferramentas que utilizamos.

Decidimos mudar do Github para o Gitlab porque achamos este mais integrado e conveniente. Necessitávamos de funcionalidades do Gitlab, como por exemplo o gitlab-ci, que é a ferramenta perfeita para build automática.

Você pode acessar os fontes do recalboxOS em https://gitlab.com/recalbox/recalbox

Descubra todos os projetos do recalbox acessando o grupo recalbox em https://gitlab.com/recalbox 


#### Revisões
Você gosta do perigo e se encherga como um early adopter (o cara que testa tudo, e antes de todos)?

Precisamos de testadores!

O Recalbox precisa de um time de testes real para revisar funcionalidades experimentais. É um grande poder... que traz grande responsabilidade. E todo mundo pode participar e nos dar feedback.

Com as novas ferramentas de desenvolvimento, os testadores terão que alterar uma linha no recalbox.conf, e atualizar seu recalbox como se fosse uma atualização padrão.

##### Lançamentos mais frequentes
Estamos cansados de esperar um ano entre cada lançamento.

Alteramos todo o processo e as ferramentas, para lançarmos novas funcionalidades uma a uma em um fluxo contínuo de melhorias. Foi uma longa jornada para tornar isto possível sem degradar sua experiência com o recalbox. Porém, está feito e desejamos que isto torne sua experiência ainda melhor.

#### Canary
No caso de (claro, não irá acontecer) um incidente com uma atualização, não desejamos que todo recalbox na terra seja transformado num peso pra papel (brickado).

Inauguramos um sistema de Lançamentos Canary. Logo que uma nova versão é finalizada, apenas uma pequena porcentagem de recalboxers será atualizado, nos deixando cientes se tudo está OK. Então, soltamos a besta.


#### Próximo?
Como seu recalbox irá atualizar com uma maior frequência, estamos criando uma nova forma de baixar e inicializar o recalboxOS. Isto irá reduzir o tempo de atualização de 50% a 80%.

Sim 80%! De fato, o processo de atualização será reduzido ao download do arquivo de atualização. Nunca mais cópias para a partição raíz durante um reboot etc.

E este é apenas o começo. Recalbox 4.1 é um grande passo no projeto, portanto, continue nos seguindo... se você não perder todo o tempo jogando alguns antigos clássicos do video game :)
