+++
date = "2017-10-13T13:37:00+02:00"
image = "/images/recalbox-4.1-is-out/recalbox-4.1-is-out.jpg"
title = "4.1 is OUT !!!"
slug = "recalbox-4.1-is-out"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "substring"
[translator]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "substring"
+++

At last ... After more than a year of development, several public unstable releases starting from February with quite much feedback from all of you, dear community, and many other changes that you wouldn't see (new blog, new infrastructure, new way of developing/building/releasing, and much more ...)

We are really glad to release this 4.1 version of Recalbox ! Here are some of the main features of this new version:

- Bluetooth has been totally reworked (and I DO mean totally rewritten) so we can support the Pi3 internal bluetooth. Please note you *MUST* pair again all your devices, that's something that can't be upgraded.
- New supported boards : Odroid XU4 and C2, PC X86 (32 and 64 bits) ... as well as the Pi0-w !
- Regarding the EmulationStation side : many new translations, contextual help, new favourite icons, update download progress percentage, Screenscraper natively supported, audio output device selection, many bugs solved ... and a virtual keyboard!
- A new web manager, thanks to DjLeChuck. Go to http://recalbox (Windows only) or http://recalbox.local (Linux and MAC)
- New drivers for XBox360, DualShock3 + DualShock4 pads supported on bluetooth (all pads generations may not be compatible)
- New systems : AppleII, DOS, PSP, Dreamcast and ... Commodore64 ! And even Gamecube and Wii (but only on PC 64 bits)? Those systems are not available on all boards. If you see no new folder in the roms directory, then the system is not supported on your board.
- New MAME emulator : advancemame 3.4 (only available on Pi for now)
- Better support for TFT screens
- No more trouble with FAT32 formatting ! Recalbox is now also available as an image format !

And many other new features that you'll discover as soon as your Recalbox is updated ! So join us on the forums to celebrate this new release and to share your feedback (and bugs ... eventually ... I hope not !)

One last thing : please be patient during the update. Recalbox will have to reboot several times, the screen can turn black for a while ... Do not worry, it's al under control ! Just let your Recalbox work !

PS : Want more ? Have a look [here](https://archive.recalbox.com/v1/upgrade/rpi3/recalbox.changelog?source=install) for the complete changelog (that will anyway be displayed once your upgrade is over), .img are available on [the complete download page](https://archive.recalbox.com).

And if you ever feel unpatient during the update and want to spoil yourself with some new features of 4.1 [have a look on the 4.1 Youtube playlist](https://www.youtube.com/playlist?list=PL2oNQ0AT7fx2LqCuw9VeK0it9ErFiYlnT).