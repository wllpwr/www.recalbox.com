+++
date = "2017-11-02T20:00:00+02:00"
image = "/images/blog/2017-11-01-recalbox-17.11.02/title-17.11.02.png"
title = "Recalbox 17.11.02"

[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"

[translator]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "substring"
+++

Hey everyone !

Although Recalbox **4.1** is not even 3 weeks old,  you've been more than 100.000 retrogamers to install or update Recalbox, and many of you made some feedback !

We promised we'd release new versions much faster than once or twice a year, time has come to prove it ! It's up to you to acknowledge our efforts that made that **4.1** be released that late !

You should now notice that we give up the old version numbering like _4.1_ to switch to a new "semantic" revision based on the release date. So this new version is the **17.11.02**, it's so much easier to manage !


Let's stop talking and have a look at the evolutions:

* New firmwares for x86 (mostly for INTEL devices)
* Moonlight now finally runs without crashing at start
* Better logs on bluetooth pairing
* Slightly increased the bluetooth scanning time
* Fixed a bug on bluetooth that applied a 8bitdo-only fix to any paired device
* The security option can now be correctly disabled
* Pad reordering now works with PPSSPP
* Moga pro pad now supported
* BlueMSX default options changed (MSX2/60Hz/ym2413=enabled)


There are still a few bugs ongoing that we'll solve a bit later:

* Advancemame only has sound ouput. This happens only for people who upgraded from 4.0.x, you can find a fix [here](https://forum.recalbox.com/topic/9522/plantage-advance-mame)
* Bluetooth pairing can be quite a struggle ... Investigating and fixing this is much more complicated and will take some time, we may need some testers feedback.


Please keep on reporting your impressions on the forum. Let me remind you that we created a new [testers' corner](https://forum.recalbox.com/category/54/testers-s-corner) there. Please pay attention to the rules in that section.


Happy update !
