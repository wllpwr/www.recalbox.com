+++
date = "2018-03-30T01:00:00+02:00"
image = "/images/blog/2018-03-30-recalbox-18-03-30/recalbox-18.03.30-banner.jpg"
title = "Recalbox 18.03.30"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"
[translator]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"

+++

Bonjour chers amis Recalboxiens !

Voici une nouvelle version qui n'apporte aucune nouveauté, mais simplement des corrections de bugs de la **18.03.16**. En voici la liste :

* **Moonlight: correction d'une mauvaise génération des GUID SDL2**. Merci à l'auteur de Moonlight d'avoir corrigé très rapidement le bug !

* **Odroid-xu4: suppression du grincement au lancement du ventilateur**. La tension appliquée était trop faible pour les ventilateurs de nouvelle génération, ce qui empêchait le ventilateur de démarrer correctement. Il sifflait de façon insupportable jusqu'à ce que la température soit assez élevée pour forcer une tension supérieure. C'est à présent résolu !

* **configgen: correction videoconfig=auto**. Un certain nombre d'entre vous avaient des problèmes de résolution lors du retour des émulateurs sous ES (en particulier avec la N64 et la Dreamcast). Grâce au retour de la communauté, c'est à présent résolu.

* **ES: correction de la transition du dernier au premier système**. Ce problème arrivait en mode bandeau, c'est maintenant aussi rapide qu'avant !

* **Theme: correction à recalbox-next**. Quelques problèmes de lisibilité à droite à gauche, c'est parfait à présent !

* **manager: correction de l'authentification et du menu**. L'authentification fonctionne à présent correctement !

* **force la création d'un fichier d'uuid s'il manque** Lézamis, évitez de jouer avec les fichiers dont vous ne connaissez pas l'utilité. Celui-ci est très utile et bloque les mises à jour s'il manque.

Il y a toujours un bug qui fait planter ES lorsque votre Recalbox a chargée pas mal de roms. Il y a une fuite de mémoire que nous devons réparer. C'est le genre de chose délicate à fixer, et cela pourrait prendre quelques versions avant que ce ne soit complètement corrigé. En attendant, s'il plante, vous pouvez redémarrez ES avec votre smartphone via le webmanager !

Un dernier mot sur le **Pi3b+** : Recalbox n'est pas encore compatible avec. Et bien qu'en interne nous avons pu faire fonctionner Recalbox dessus (pas de sortie publique encore, ne demandez pas où le télécharger), son support demande une mise à jour majeure de l'OS sur laquelle je travaille depuis 3 mois. Cette nouvelle version ne peut être diffusée car elle va à coup sûr crasher les installations sur Pi et PC. On travaille donc sur une nouvelle procédure qui semble bien marcher, donc soyez patients. Vous savez tous que nous ne sommes pas les plus rapides pour porter Recalbox sur des nouveaux Pi ;) Mais rassurez-vous : ça ne prendra pas aussi longtemps que le support du Bluetooth du Pi3 !

Enfin : bien que cette version soit peu avant les poissons d'Avril, il n'y a pas de vidéo d'intro "Bonne année !", pour ceux à qui le bonhomme de neige aurait manqué ;)
