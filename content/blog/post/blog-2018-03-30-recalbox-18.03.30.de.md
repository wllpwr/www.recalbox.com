+++
date = "2018-03-30T01:00:00+02:00"
image = "/images/blog/2018-03-30-recalbox-18-03-30/recalbox-18.03.30-banner.jpg"
title = "Recalbox 18.03.30"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"
  
[translator]
  github = "https://github.com/Nachtgarm"
  gitlab = "https://gitlab.com/Nachtgarm"
  name = "Nachtgarm"

+++

Hallo liebe Retrogamer!

Diese Version bringt keine neuen Features, sondern nur einige Bugfixes für Version **18.03.16**. Hier ist die Liste:

* **Moonlight: fix schlechte SDL2 GUID Erzeugung**. Dank geht an den Autor von Moonlight, der den Fehler sehr schnell behoben hat!

* **Odroid-xu4: entfernen des Lüfterlärms beim hochfahren**. The Spannung ist zu niedrig für die aktuellsten Lüfter. Dadurch konnte der Lüfter nicht richtig starten. Es pfiff mit einem lästigen Geräusch, bis die Temperatur hoch genug war, um eine höhere Spannungsstufe zu erreichen. Das ist jetzt behoben!

* **configgen: fix videoconfig=auto**. Viele von euch hatten Probleme, als ihr aus einigen Emulatoren (hauptsächlich N64 und Dreamcast) zurück ins Menü gegangen seid, dass ES gestreckt oder verzerrt angezeigt wurde. Dank des Community-Feedbacks ist dies gelöst.

* **ES: fix langsamer Übergang vom letzten zum ersten System**. Das ist jetzt so schnell, wie vorher!

* **Theme: verschiedenste fixe für recalbox-next**. Einige Lesbarkeitsprobleme hier und da, aber jetzt ist es perfekt!

* **manager: fix Anmeldeinformationen und Menü**. Die neue Sicherheitsoption, die eine Authentifizierung im Web-Manager erforderte, funktioniert jetzt!

* **die Erstellung einer uuid-Datei erzwingen, falls diese fehlt** Leute, bitte legt euch nicht mit Dateien an, für die ihr nicht wisst, wofür sie bestimmt sind. Diese hier ist sehr nützlich und verhindert ein Update, wenn es fehlt.

Es gibt immer noch einen Fehler in Bezug auf ES, das abstürzt, wenn eure Recalbox stark ROMs-beladen ist. ES stürzt nach mehreren Spielstarts ab. Es gibt ein Speicherleck, das wir stopfen müssen. Das ist eine ziemlich aufwändige Aufgabe, dies zu debuggen und könnte ein paar Releases dauern. In der Zwischenzeit startet ES einfach mit eurem Handy auf der Webmanager-Seite neu!

Ein letztes Wort zum Thema **Pi3b+**: Recalbox ist noch nicht kompatibel. Obwohl wir es auf unserer Seite am laufen haben (noch keine öffentliche Version, fragt nicht nach Links), erfordert es ein größeres Betriebssystem-Upgrade, an dem ich bereits seit 3 Monaten arbeite. Wir können es noch nicht veröffentlichen, da ein Upgrade auf diese neue Version eure Pi und x86 Installationen zerstören würde. Wir arbeiten an einem neuen Upgrade-Verfahren, das auf unserer Seite in Ordnung zu sein scheint. Wie üblich, habt Geduld. Ihr wisst alle, dass wir Recalbox nicht so schnell an neue Raspberry-Boards anpassen können ;) Aber es wird nicht so lange dauern, wie die Pi3-interne Bluetooth-Unterstützung!

Und schließlich: Obwohl diese Veröffentlichung kurz vor dem 1. April ist, haben wir kein "Happy New Year" Splash-Video aufgenommen, für diejenigen von euch, die das Snowman-Video vermissen ;)
