+++
date = "2017-10-13T13:37:00+02:00"
image = "/images/recalbox-4.1-is-out/recalbox-4.1-is-out.jpg"
title = "4.1 IST DA !!!"
slug = "recalbox-4.1-is-out"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "substring"
[translator]
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
  name = "Robsdedude"
+++

Endlich... Nach mehr als einem Jahr Entwicklung, mehreren öffentlichen unstable Releases seit Februar mit viel Feedback von euch allen, liebe Community, und vielen anderen Änderungen, die ihr gar nicht seht (neuer Blog, neue Infrastruktur, neue Methoden des Entwickelns/Bauens/Veröffentlichens und viele mehr...)

Wir sind wirklich froh Recalbox Version 4.1 zu veröffentlichen! Hier sind die größten Änderungen der neuen Version:

- Bluetooth wurde völlig überarbeitet (und ich mein damit komplett neu geschrieben), damit wir das interne Bluetooth-Modul des Pi3 unterstützen können. Bitte beachtet, dass all eure Bluetoothgeräte neu gekoppelt werden *MÜSSEN* ("pairing"). Das kann leider nicht automatisch geupgradet werden.
- Neue unterstützte boards: Odroid XU4 und C2, PC X86 (32 und 64 Bit)... sowie der Pi0-w!
- Bezüglich EmulationStation: Viele neue Übersetzungen, Hilfe-Pop-ups, Favoriten-Icons, Update-Download Fortschrittsanzeige, native Screenscraper-Unterstützung, Auswahl des Audio-Ausgabegeräts, viele beseitigte Bugs... und eine virtuelle Tastatur!
- Ein neuer Web-Manager, dank DjLeChuck. Geht dazu auf http://recalbox (unter Windows) oder http://recalbox.local (unter Linux und MAC)
- Neue Treiber für XBox360, DualShock3 + DualShock4 Controller über Bluetooth (möglicherweise sind nicht alle Generationen kompatibel)
- Neue Systeme: AppleII, DOS, PSP, Dreamcast und... Commodore64! Sogar Gamecube und Wii (nur für PC 64 Bit)!
Diese Systeme sind leider nicht für alle Architekturen verfügbar. Wenn ihr keinen neuen Ordner im roms Verzeichnis findet, wird das System von eurer Architektur nicht unterstützt.
- Neuer MAME Emulator: advancemame 3.4 (bisher nur für Pi verfügbar)
- Bessere Unterstützung von TFT-Monitoren
- Kein Ärger mehr mit dem FAT32-Formatieren! Recalbox gibt es jetzt auch im Image-Format!

Es gibt noch viele weitere Features, die ihr entdecken werdet, sobald eure Recalbox aktualisiert ist! Schaut im Forum vorbei um die neue Version zu feiern und euer Feedback zu teilen (oder bugs... womöglich... Ich hoffe nicht!)

Eine letzte Sache noch: Bitte habt während des Updates Geduld. Recalbox wird mehrmals neu starten. Das Bild kann zeitweise nur schwarz sein... Keine Sorge, es ist alles unter Kontrolle! Lasst eure Recalbox das einfach machen!

PS: Mehr gefällig? [Hier](https://archive.recalbox.com/v1/upgrade/rpi3/recalbox.changelog?source=install) findet ihr eine Liste aller Änderungen (die wird nach dem Update sowieso angezeigt). Die Images stellen wir [hier zum download](https://archive.recalbox.com) bereit.

Und falls euch das Update zu lange dauert und ihr euch die Überraschung einiger neuer Features von 4.1 vorwegnehmen möchtet, [schaut euch die 4.1 YouTube-Playlist an](https://www.youtube.com/playlist?list=PL2oNQ0AT7fx2LqCuw9VeK0it9ErFiYlnT).
