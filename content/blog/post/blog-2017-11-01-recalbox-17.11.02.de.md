+++
date = "2017-11-02T20:00:00+02:00"
image = "/images/blog/2017-11-01-recalbox-17.11.02/title-17.11.02.png"
title = "Recalbox 17.11.02"

[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"
[translator]
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
  name = "Robsdedude"
+++

Hallo ihr Lieben!

Obwohl Recalbx **4.1** nicht mal 3 Wochen alt ist, haben über 100.000 von euch Retrogamern die neue Version heruntergeladen oder als Update installiert. Und viele von euch haben uns Feedback gegeben!

Wir haben versprochen, dass wir neue Versionen viel schneller als ein-, zweimal pro Jahr veröffentlichen würden. Es ist Zeit für den Beweis! Es bleibt euch überlassen unsere Anstrengungen zu würdigen, die dafür verantwortlich sind, dass **4.1** so spät veröffentlicht wurde.

Beachtet, dass wir das alte Versionierungsschema (wie z.B. _4.1_) aufgegeben haben und zu einer neuen "semantischen" Versionierung auf Basis des Erscheinungsdatums gewechselt haben. Diese neue Version heißt also **17.11.02**, das ist viel einfacher zu handhaben!


Schluss mit dem gequatsche. Schauen wir uns die Entwicklungen an:

* Neue Firmwares für x86 (hauptsächlich INTEL-Geräte)
* Moonlight läuft nun endlich ohne beim Start abzustürzen
* Bessere Logs beim Paaren mit Bluetooth-Geräten
* Etwas schnellere Bluetooth-Scans
* Bluetooth-Fehler behoben, der einen Fix ausschließlich für 8bitdo bei allen gepaarten Geräten angewendet hat
* Die Sicherheitsoption kann jetzt korrekt deaktiviert werden
* Pad-Umsortierung funktioniert nun mit PPSSPP
* Moga Pro Pad wird unterstützt
* BlueMSX Default-Einstellungen geändert (MSX2/60Hz/ym2413=enabled)


Es gibt noch ein paar weiter Bugs, die wir sehr bald entfernen werden:

* Advancemame spielt nur Ton ab. Das betrifft nur Nutzer, die von 4.0.x aktualisiert haben. Einen Fix könnt ihr [hier](https://forum.recalbox.com/topic/9522/plantage-advance-mame) finden.
* Bluetooth Pairing kann ein Krampf sein... Die Fehlerquelle zu lokalisieren und zu beseitigen ist sehr viel komplizierter und wird uns einige Zeit kosten. Hier könnten wir Feedback von Testern brauchen.



Bitte berichtet eure Erfahrungen weiterhin im Forum. Lasst mich euch daran erinnern, dass wir eine neue [testers' corner](https://forum.recalbox.com/category/54/testers-s-corner) (englisch) erstellt haben. Bitte beachtet die Regeln dort.

Fröhliches Updaten!