+++
date = "2017-02-22T16:24:57+02:00"
image = "/images/blog/2017-02-11-bootoptions/title-boot-options.png"
title = "Customize the boot behavior of your recalbox!"
disqus_identifier = "blog-2017-02-22-bootoptions"
draft = false
[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"

+++

<div><p dir="auto">The new release of recalbox brings to you a new level a customization.</p><p dir="auto">Today we are going to see how you can decide exactly what happens when you boot the recalbox!</p><p dir="auto">By going in the <strong>ADVANCED SETTINGS</strong>, you will see a <strong>BOOT SETTINGS</strong> entry :</p><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets//images/blog/2017-02-11-bootoptions/advanced-settings.jpg" target="_blank"><img src="/images/blog/2017-02-11-bootoptions/advanced-settings.jpg" alt=""></a></div><p dir="auto">In this menu you can set option to modify the boot behavior :</p><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets//images/blog/2017-02-11-bootoptions/boot-settings.jpg" target="_blank"><img src="/images/blog/2017-02-11-bootoptions/boot-settings.jpg" alt=""></a></div><p dir="auto">Let's see what it means:</p><ul dir="auto"><li> <strong>KODI AT START</strong> : boot on kodi, and access emulationstation only when quitting kodi.</li><li> <strong>GAMELIST ONLY</strong> : only show games contained in the gamelist.xml file (located in your roms directories). This option highly speeds up boot time when you have scraped games, but new games will not be detected.</li><li> <strong>BOOT ON SYSTEM</strong> : select which system to show when the recalbox frontend starts. The default value is 'favorites'. Combined with BOOT ON GAMELIST it shows the system gamelist on boot.</li><li> <strong>BOOT ON GAMELIST</strong> : show the list of games of the selected system rather than the system view.</li><li> <strong>HIDE SYSTEM VIEW</strong> : hide the system view. The system view is the view where you see available systems logos. You will start on a gamelist and cannot browse through systems. Usefull for bartop and cabinets, or if you want to fix a system for you childs. This option automatically enables BOOT ON GAMELIST</li><li> <strong>FORCE BASIC GAMELIST VIEW</strong> : only show basic gamelist, even if scrap information is found in gamelist.xml files</li></ul><p dir="auto">And here is a video on how it looks like:</p><div style="text-align: center"><iframe width="560" height="315" src="https://www.youtube.com/embed/W2C1OO_fvSE" frameborder="0" allowfullscreen="">video</iframe></div><p dir="auto">Have fun!</p> </div>