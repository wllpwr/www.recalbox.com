+++
date = "2019-11-15T07:00:46Z"
title = "Lançado o Recalbox 6.1.1"
image = "/images/blog/2019-11-15-recalbox-6.1.1-dragonblaze-released/Software_Box_Mockup_1_RECALBOX.jpg"

[author]
  name = "OyyoDams"
  github = "https://github.com/OyyoDams"
  gitlab = "https://gitlab.com/OyyoDams"
  facebook = "https://www.facebook.com/oyyodams/"
  twitter = "https://twitter.com/OyyoDams"
  
[translator]
  name = "Nícolas Wildner"
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
+++

O Recalbox está cada vez melhor!

Desde o lançamento da versão 6.0 vocês continuam nos agradecendo, mas também continuam nos reportando para que possamos aprimorar o projeto Recalbox um pouco mais a cada dia. Este é o resumo de todas as últimas alterações !


## Recalbox 6.1: DragonBlaze

A versão 6.0 foi uma revolução, mas como ainda queremos fazer mais, esta nova versão entitulada "6.1" traz uma pequena quantidade de coisas novas e eleva o número de sistemas emulados no Recalbox para um total de quase 95, adicionando mais ou menos 15 novos sistemas ! Inclui: Uzeboxes, Amstrad GX4000, Apple IIGS, Amiga 600 & 1200, Spectravideo, Sharp X1, Palm, PC-88, TIC-80, Thomson MO6, Olivetti Prodest PC128, MSXturboR, Multivision, e também os bastante antecipados sistemas de arcade Atomiswave, NAOMI e também o Saturn* (*Disponível apenas para versão PC até o momento.)

O Recalbox 6.1 também marca a chegada dos aclamados snaps de vídeo, pequenas visualizações de vídeo que são iniciadas automaticamente quando você fica em um jogo da sua lista. Esse recurso é ideal para permitir que os usuários descubram determinados títulos e tenham idéia da jogabilidade de forma rápida!

O Recalbox 6.1 traz também o tão esperado retorno do scraper interno (programa para obter todos os metadados de um jogo como capa, informações, descrição ...), equipado com novas opções!

Além da família de Raspberry Pi Zero-WH, 1, 2, 3B, 3B+ e Compute Module 3, Odroid C2 e XU4 e x86 / x64, o Recalbox 6.1 agora também pode ser instalado no mais recente Raspberry Pi 3A+, uma boa notícia para os makers!

Resultado de um longo período de desenvolvimento e otimização, o Gpi Case agora é 100% plug and play no Recalbox 6.1. Não há necessidade de instalar scripts ou fazer configurações, tudo é automático e totalmente otimizado para maximizar o desempenho modesto do Pi0 / 0W! Para isso, o Recalbox 6.1 passou por um emagrecimento similar a de um treinamento militar!

O melhor de tudo isso? TODOS OS CARTÕES se beneficiam das melhorias e otimizações feitas para o Gpi Case. Em diversos cartões, o tempo de inicialização foi reduzido quase a um instante, e houve um aprimoramento global na velocidade, responsividade e leveza... pois... quem perde tempo mínimo com detalhes, faz o máximo com o que realmente importa !

Mudanças sem ordem de relevância :

    • RetroArch foi atualizado para a versão 1.7.8, deixando a interface horrenda RGUI de lado em prol da nova e moderna OZONE ! 
    • … e introduziu também a versão inicial das traduções em tempo real, com a função "texto-para-fala"* onde pela primeira vez você poderá ouvir uma voz auxiliando em jogos retro ! (*configuração necessária)
    • Configuração dos controles Bluetooth ficou mais fácil do que nunca
    • Suporte a arquivos ".7z" em diversos novos emuladores.
    • Melhorias na equalização do áudio entre os jogos e o menu do Recalbox
    • FinalBurn Alpha virou FinalBurn Neo, seguindo o abandono do projeto. (Romset ainda recomendado : 0.2.97.44, sem alteração)
    • MAME evoluiu e virou "mame2003_plus". Apesar de completamente retrocompatível com o Mame 0.78 a romset recomendada é a "2003+", que adiciona compatibilidade para mais 500 jogos de arcade!
    • Modo DEMO aprimorado, com diversas novas configurações como a escolha de sistemas e também uma tela final para relembrar ao jogador qual o título do jogo que ele acabou de jogar, para caso ele deseje jogar novamente !


## Recalbox 6.1.1: DragonBlaze

Sejamos honestos, não experem alterações grandes para o Recalbox 6.1.1.
Com todas as alterações que trouxemos na versão 6.1, e mesmo com nossos (diversos) inúmeros testes, vocês reportaram problemas de instabilidade e bugs.
O foco é a resolução destes bugs para estabilizar o Recalbox 6.1 com este bugfix que chamamos te "6.1.1", antes de implementar novas surpresas para a 6.2 : mas esta é outra história, por favor, tenham paciência !

Estamos reestruturando toda a parte de suporte a Wi-Fi e Bluetooth.
Conectar a novas redes sem fio será mais fácil e estável do que nunca !

Corrigimos diversos bugs pequenos no XU4, PC, tambem na utilização de alguns emuladores como SNES, GX4000, Apple2, atari, …

Também nos aproveitamos da situação para atualizar diversos emuladores para que vocês façam uso da última tecnologia disponível, com diversos aprimoramentos menores.


## Download

Quer baixar o Recalbox 6.1.1 DragonBlaze?

[Baixe aqui](https://archive.recalbox.com) ! É gratuito, open-source, mantido por uma equipe de pessoas benevolentes e por um exército de voluntários com um objetivo NÃO-COMERCIAL (só por diversão !)