+++
date = "2019-03-22T11:54:12Z"
image = "/images/blog/2019-03-22-upcoming-stable-release/banner.jpg"
title = "Upcoming stable release"

[author]
  name = "Fab2Ris"
  facebook = "https://www.facebook.com/fabriceboutard"
  twitter = "https://twitter.com/Fab2Ris"
[translator]
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
  name = "Robsdedude"
+++

Tolle Neuigkeiten!

Die nächste stabile Version von Recalbox ist nur ein paar Tage entfernt!

Vorher brauchen wir aber eure Hilfe!
Helft uns, vor dem finalen Release, kritische Bugs zu finden, die wir übersehen haben.

## Versionierungsschema

Sicherlich habt ihr bemerkt, dass wir vor ein paar Versionen das Namensschema der Versionen geändert haben. Zuvor verwendeten wir Nummern (`4.0`, `4.1`, …), wechselten dann aber zu zeitbasierten Versionsnamen (`18.04.20`, `18.06.27`, …) anstatt mit `5.x` weiterzumachen.

Dummerweise sind diese zeitbasierten Namen schwer zu lesen, zu schreiben und auszusprechen. Kurz um, sie sind nicht Nutzerfreundlich.

Deshalb haben wir uns entschlossen wieder zu **Nummern zurückzuwechseln**. Diese sind leichter zu lesen und semantischer. Anhand der Versionsnummer kann man ablesen, wie wichtig ein Update ist, während das Release-Datum deutlich weniger relevante Informationen vermittelt.

Wir werden alle zeitbasierten Versionen behandeln, als wären sie `5.x` gewesen und mit dem nächsten stable Release bei `6.0` weitermachen 🎉.

Wie Kindern, werden wir den künftigen Releases vermutlich Namen geben 😉. Für die nächste Version ist die Entscheidung noch nicht gefallen, aber sie wird definitiv einen Namen bekommen! ❤️


## Release Candidate 1

Unser Entwickler-Team hat in den letzten Monaten so viele großartige Features, neue Emulatoren, Updates, Themes, Systeme, Hardware-Unterstüzungen und mehr umgesetzt. In diesem Post werden wir uns nicht mit diesen aufregenden Neuerungen befassen. Ihr könnt aber sicher sein, dass es einen detalierten Blog-Post zum finalen Release geben wird, wenn dieses bald erscheint.


Wir haben so viel neu gecodet, dass uns das notwendige, ausgiebige Testen aller Optimierungen die letzten Monate in Beschlag genommen hat. Alles, um eich ein möglichst reibungsfreies Erlebnis zu bieten.

Das alles ist aber nur danke euch möglich!

Mit der Hilfe unsere großartigen Community (unterstützt von unserem nicht minder großartigen Support-Team) konnten wir (bisher) drei verschiedene _beta_ Versionen veröffentlichen, danke derer dutzende neue Bugs auffielen, die unsere Entwickler so schnell wie sie konnten behoben haben. Schließlich besteht unser Team ausschließlich aus Ehrenamtlichen.

Rückblickend war es ein langer Weg bis zum Release dieser neuen Recalbox-Version!

Zum heutigen Tag sind wir so stolz auf und zufrieden mit unserer Arbeit, dass wir sie, so wie sie ist, veröffentlichen könnten.

Aber das reicht uns nicht! Wir möchten sicher stellen, dass unsere nächste stabile Version auch euren Erwartungen entspricht: Sie muss stabil laufen 💪

Deshalb **veröffentlichen wir heute einen Release Candidate (RC)**. Wie der Name schon vermuten lässt, ist ein RC eine Version, die ein guter Kandidat für das final Release ist. Der RC erscheint ein paar Tage vor dem final Release, sodass er auf einer breiteren Palette an Hardware, Setups, _BIOSes_ und _ROMs_ getestet werden kann, um mögliche kritische Bugs zu entdecken, die wir übersehen haben 😬

Wie euch vielleicht bekannt ist, richten sich _beta_ Versionen an fortgeschrittene Nutzer, die wissen, wie man Bugs findet, reproduziert und korrekt meldet (mit log Auszügen). Auf der anderen Seite soll ein Release Candidate auch von mittelerfahrenen Nutzern verwendbar sein, die Bugs reproduzieren und melden können (was an sich schon nicht trivial ist). Er ist nicht für euch geeignet, wenn ihr nicht wisst, wie man eine SD-Karte flasht, ein paar Spiele testet oder einen Controller konfiguriert.

**Was wir von euch erwarten**, wenn ihr helfen wollt: Ladet euch das passende [Image](https://recalbox-releases.s3.nl-ams.scw.cloud/stable/index.html) zu eurer Hardware herunter, konfiguriert es nach euren Wünschen (Optionen, Spiele, Themes, …) oder übernehmt eure bisherige Konfiguration und lasst uns wissen, wenn etwas nicht funktioniert💥

Wenn ihr kritische Probleme findet, meldet euch bei uns über [Discord](https://discord.gg/d2xCQ4e) und liefert uns so viele Details, wie möglich. Das macht es uns leichter den Fehler zu finden und zu beseitigen. Hier eine Liste mit für uns nützlichen Informationen:

* Recalbox Version (es gibt womöglich mehrere Release Candidates)
* Benutzte Hardware: Board und Controller
* Das verwendete Theme
* Änderungen an der Standard-Konfiguration
* Detalierte Schritte, um den Bug zu reproduzieren
* Erwartetes Verhalten
* Tatsächlich beobachtetes Verhalten

Einige Probleme werden nicht zur nächsten stabilen Version behoben. Ihr könnt sie dennoch melden; wir werden uns aber erst später darum kümmern:

* Spezifische Controller-Probleme
* Spezifische _ROM_-Probleme
* Spezifische Theme-Probleme
* Bekannte Bugs/Issues (die in einer späteren Version behoben werden):
  * Cheats funktionieren nicht mit Mame2010
  * Retoarch Seitenverhältnis ist nicht mit ES/Configgen synchronisiert
  * Kodi startet nicht mit manchen Controllern


## Upgrade (leider nein!)

Wenn ihr bereits Recalbox mit der letzten stabilen Version oder einer unserer letzten _beta_ Versionen verwendet und auf einen RC via in-app Update-System upgraden wollt: **tut es nicht!**

Wir haben Recalbox derart tiefgehend umgestaltete, dass das Update-System und die dazugehörige Infrastruktur nicht mehr Kompatibel sind. Es gibt leider keinen automatischen Weg von einer vorigen Version auf diese zu updaten.

Updates von diesem RC auf künftige RCs oder stable Releases sollten aber funktionieren.

Bitte **installiert Recalbox von Grund auf neu**, testet, spielt, hackt, habt Spaß… und lasst uns wissen, wie es läuft🤗
