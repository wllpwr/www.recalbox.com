+++
date = "2017-02-18T15:56:54+02:00"
image = "/images/blog/2017-02-18-download/title.jpg"
title = "Vous avez cassé nos serveurs de téléchargement !! :D"
disqus_identifier = "blog-2017-02-18-download"
draft = false
[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"

+++

<div><p>Bonjour à tous,<br>Comme vous pouvez le voir, nos serveurs de mise à jour sont actuellement surchargés. Vous êtes trop nombreux a mettre à jour votre Recalbox, les serveurs ont trop de connexions en même temps.<br>Nous n'étions pas préparés à celà, nous n'avions pas ce genre de problèmes avec les sorties précédentes.<br>Nous avons donc redimensionné les serveurs et ajouté des mirroirs. Une fois le cache rempli sur ces mirroirs, la situation devrait revenir à la normale.<br>Merci de votre patience et votre compréhension<br>P.S.: Pour information, quand votre Recalbox est en veille, le téléchargement continue. Mais une fois terminé, l'écran ne se rafraîchi pas pour afficher le message de redémarrage.<br>Alors de temps en temps, bougez votre manette pour sortir la Recalbox de veille ;)<br><br></p><p>Vous pouvez suivre l'avancé du problème sur twitter : <a href="https://twitter.com/recalbox">@recalbox</a></p></div>