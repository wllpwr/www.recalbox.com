+++
date = "2017-12-03T11:00:00+02:00"
image = "/images/blog/2017-12-03-recalbox-17-12-02/recalbox-17.12.02-banner.jpg"
title = "Recalbox 17.12.02"

[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  twitter = "https://twitter.com/digitalumberjak"
  name = "digitalLumberjack"

[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"

+++

Olá!

Lançamos a versão `17.12.01` na última sexta-feira e algumas pessoas detectaram lags durante a seleção de jogos e até mesmo ao jogar.

Isto foi causado pelo serviço steam controller configurado para sempre iniciar no boot, mesmo que não haja um controlador da steam conectado.

Nós corrigimos este comportamento, e você pode obter tal correção ao atualizar para o recalbox `17.12.02`.

Para uma nova instalação, as imagens do recalbox estão disponíveis em [archive.recalbox.com](https://archive.recalbox.com)