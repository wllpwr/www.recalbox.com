+++
date = "2017-03-05T16:39:32+02:00"
image = "/images/blog/2017-03-05-hidden/title-showhidden.jpg"
title = "Cachez vos jeux!"
disqus_identifier = "blog-2017-03-05-hidden"
draft = false

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
  
+++
<p dir="auto">Aujourd’hui découvrons une autre amélioration en 4.1.0 d'une feature apporté en 4.0.0: <strong>cacher les jeux</strong> dans EmulationStation.</p><p dir="auto">"Hidden files" est une feature qui vous permet de "cacher" des jeux, ou des fichiers spécifiques comme les bios etc... Cependant jusqu’à aujourd'hui, une fois vois jeux cachés, ils disparaissaient d'EmulationStation tant que vous n’éditiez pas le fichier gamelist.xml correspondant, à la main...</p><p dir="auto">Nous avons donc décidé d'ajouter <strong>une option pour afficher les jeux cachés</strong>. Une fois cette option activée, les jeux cachés sont ré affichés dans EmulationStation et sont précédés par cette icone:</p><img src="/images/blog/2017-03-05-hidden/eye_closed.jpg" alt="eye_closed"><p dir="auto">Voici un exemple:</p><p dir="auto"></p><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets/images/blog/2017-03-05-hidden/es_showhidden.jpg" target="_blank"><img src="/images/blog/2017-03-05-hidden/es_showhidden.jpg" alt="es_showhidden"></a></div><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets/images/blog/2017-03-05-hidden/es_showhidden_2.jpg" target="_blank"><img src="/images/blog/2017-03-05-hidden/es_showhidden_2.jpg" alt="es_showhidden_2"></a></div><p dir="auto">Et voici une petite vidéo qui vous montre le resultat:</p><iframe width="560" height="315" src="https://www.youtube.com/embed/I9v-4OsDUZQ" frameborder="0" allowfullscreen=""></iframe>