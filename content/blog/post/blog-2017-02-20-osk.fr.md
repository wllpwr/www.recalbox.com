+++
date = "2017-02-20T16:09:05+02:00"
image = "/images/blog/2017-02-20-osk/recalbox-blog-osk-title.jpg"
title = "Clavier Virtuel (OSK)"
disqus_identifier = "blog-2017-02-20-osk"
draft = false

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"

+++

<div><p dir="auto">Cette nouvelle version de recalbox sera encore plus conviviale.</p><p dir="auto">Qui n'a jamais été dans la situation de devoir chercher désespérément un clavier USB pour saisir ses réglages WIFI, ou modifier les métadonnées d'un jeu ? </p><p dir="auto">Aujourd'hui, avec cette nouvelle version de recalboxOS, ce temps est révolu. </p><p dir="auto">Vous pourrez utiliser votre manette et le nouveau <strong>clavier virtuel (OSK)</strong> présent dans EmulationStation.</p><p dir="auto"></p><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets//images/blog/2017-02-20-osk/recalbox-osk.jpg" target="_blank"><img src="/images/blog/2017-02-20-osk/recalbox-osk.jpg" alt="es_osk"></a></div><p dir="auto">Voici une courte vidéo de démonstration :</p><div style="text-align: center"><iframe width="560" height="315" src="https://www.youtube.com/embed/LeWHVSflngU" frameborder="0" allowfullscreen="">video</iframe></div></div>