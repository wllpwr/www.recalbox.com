+++
date = "2018-03-30T01:00:00+02:00"
image = "/images/blog/2018-03-30-recalbox-18-03-30/recalbox-18.03.30-banner.jpg"
title = "Recalbox 18.03.30"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"

+++

Howdy fellow recalboxers!

This version brings no new feature, but only some bugfixes of **18.03.16**. Here is the list:

* **Moonlight: fix bad SDL2 GUID generation**. Thanks to the Moonlight author who solved this bug very fast!

* **Odroid-xu4: remove screeching noise at fan startup**. The voltage is too low for new generation fans. This prevented the fan from starting properly. It was whistling with an annoying sound until the temperature would be high enough to reach a higher voltage step. This is now fixed!

* **configgen: fix videoconfig=auto**. Quite a number of you had problems when coming back from some emulators (mainly N64 and Dreamcast) with a stretched out ES. Thanks to the community feedback, this is solved.

* **ES: fix slow transition from last to first system**. It's now as fast as before!

* **Theme: various fixes to recalbox-next**. Some readability problems here and there, but it's perfect now!

* **manager: fix credentials and menu**. The new security option that required authentication on the web manager is now woking!

* **force the creation of a uuid file if it's missing** Guyz, please do not mess with files you don't know what they're meant for. This one is very useful and prevents any update if it's missing.

There is still a bug regarding ES that crashes when your Recalbox is heavily roms-loaded. ES crashes after several game launches. There is a memory leak we need to fix. That's the kind of very annoying thing to debug, and could eventually take a few releases before it is fixed. Meanwhile, just restart ES with your mobile phone on the webmanager page!

One last word regarding **Pi3b+**: Recalbox is not yet compatible with it. Although we have it working on our side (no public release yet, don't ask for links), it requires a major OS upgrade which I've been working on for 3 months already. We can't release it yet as upgrading to this new version would break your Pi an x86 installations. We're working a new upgrade procedure that seems fine on our side. As usual, be patient. You all know we're not that fast to adapt Recalbox to new Raspberry boards ;) But it won't take as long as the Pi3 internal bluetooth support!

Finally: despite this release being just before April's fool day, we haven't included any "Happy new Year" splash video, for those of you missing the snow man video ;)
