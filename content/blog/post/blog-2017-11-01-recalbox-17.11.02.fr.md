+++
date = "2017-11-02T20:00:00+02:00"
image = "/images/blog/2017-11-01-recalbox-17.11.02/title-17.11.02.png"
title = "Recalbox 17.11.02"

[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"
+++

Bonjour à tous !

La sortie de la **4.1** est encore toute récente,  vous avez été plus de 100 000 a mettre a jour ou installer recalbox 4.1 et nombreux sont ceux qui nous ont fait des retours sur cette version !

On vous avait promis un cycle de développement plus court et moins d'un mois après la sortie de la **4.1** le moment est venu de vous le prouver. A vous de juger si nos efforts qui ont tant retardé la **4.1** en valaient la peine.

Vous noterez au passage qu'on laisse tomber le concept de numéro en _4.1_ pour passer adopter une "sémantique" basée sur la date de sortie de la version. Cette nouvelle version est donc la **17.11.02**, c'est plus simple pour s'y retrouver !


Ne perdons plus de temps, et listons les évolutions de cette version :

* Ajout de firmwares supplémentaires sur x86 (pour périphériques INTEL essentiellement)
* Correction d'un crash systématique de Moonlight
* Amélioration des logs lors de l'appairage en bluetooth
* Augmentation de la durée de recherche des pads en bluetooth
* Correction d'un bug sur le bluetooth qui forçait un hack spécial 8bitdo pour tous les pads
* Correction d'un bug sur la désactivation du mode sécurité
* Correction de l'ordre des pads sur PPSSPP
* Ajout du support des manettes Moga Pro
* BlueMSX changement des options de base (MSX2/60Hz/ym2413=enabled)


On a encore des corrections de bugs dans la besace pour les versions suivantes :

* Advancemame n'a que le son: ça arrive uniquement pour les utilisateurs ayant mis à jour depuis la 4.0.x, la résolution est disponible dans [ce sujet](https://forum.recalbox.com/topic/9522/plantage-advance-mame)
* Des difficultés à appairer par bluetooth ... L'investigation est beaucoup plus longue et compliquée, pour cela qu'on peut solliciter des testeurs.

Continuez à nous faire remonter vos impressions sur le forum qui est le lieu privilégié pour le support. J'en profite pour vous rappeler la création de la [section des testeurs]( https://forum.recalbox.com/category/54/testers-s-corner) (En anglais seulement, svp). Merci de bien lire les règles de cette section !


Bonne mise à jour !
