FROM alpine:3.9

ENV HUGO_VERSION=0.78.1

RUN apk add --update --no-cache su-exec \
 && { \
      echo '#!/bin/sh'; \
      echo 'su-exec $(stat -c "%u:%g" /src) "$@"'; \
    } > /usr/bin/entrypoint \
 && chmod +x /usr/bin/entrypoint

RUN wget --quiet --output-document - \
      https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz \
  | tar --extract --gzip --directory /usr/bin/ hugo

EXPOSE 1313/tcp

WORKDIR /src

ENTRYPOINT ["entrypoint", "hugo"]

CMD ["serve", "--bind", "0.0.0.0", "--log", "--watch", "--baseURL", "http://localhost:1313", "--buildDrafts"]
